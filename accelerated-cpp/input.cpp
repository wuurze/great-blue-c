#include <iostream>
#include <string>

int main() {
    std::cout << "Please enter your first name: ";
    std::string name; //define name
    std::cin >> name; //direct cin stream to name

    std::cout << "Hello, " << name << "!" << std::endl;
    return 0;
}
